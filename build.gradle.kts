plugins {
    java
}

/**
 * In Kotlin, we can define tasks statically. This allows
 * for IDE refactoring support such as rename.
 */
val hello by tasks.creating {
    doLast {
        print("hello ")
    }
}

// And because Kotlin is strongly typed, we can also
// use the IDE to discover what configuration options we
// have for the task.
val world by tasks.creating {
    dependsOn(hello)
    doLast {
        println("world")
    }
}

// If we specify a specialized task, such as copy,
// we now have access to its unique configuration fields.
val copyJavaSources by tasks.creating(Copy::class) {
    from("src/main/java")
    into(buildDir)
}

// For rules, you'll notice a subtle difference. Instead of passing the
// taskName as a parameter, it is now used as the receiver (in other
// words, the `this` for the lambda).
tasks.addRule("Pattern: hello<who>: Send personalized greeting") {
    val taskName = this
    if (taskName.startsWith("hello")) {
        task(taskName) {
            dependsOn(hello)
            doLast {
                println(taskName.removePrefix("hello"))
            }
        }
    }
}

// PROTIP: Add at least one repository to the buildscript classpath
// and get access to the Kotlin stdlib sources and documentation
//buildscript {
//    repositories {
//        jcenter()
//    }
//}
